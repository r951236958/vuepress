module.exports = {
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public',
    // 主題配置說明 https://www.vuepress.cn/default-theme-config
    themeConfig: {
        //儲存庫
        repo: 'r951236958/vuepress',
        nav: [
        //nav導航列連結範例
        //  { text: 'Home', link: '/' },
        //  { text: 'Guide', link: '/guide/' },
        //  { text: 'External', link: 'https://google.com' },
        //]
        
        //將nav設置為下拉選單
        {
            text: '語言',
            items: [
              { text: '中文', link: '/language/chinese' },
              { text: 'Japanese', link: '/language/japanese' }
            ]
          }
        ]
      }
}